import React from 'react'
import { View, Text, StyleSheet } from 'react-native' 

import withBounce from '../hocs/withBounce'

const Error = ({ text }) => (
  <View style={styles.container}>
    <View style={styles.textContainer}>
      <Text style={styles.text}>{text}</Text>
    </View>
    <View style={styles.arrow}/>
  </View>
)

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    alignItems: 'center', 
    zIndex: 100000,
    marginBottom: -15,
  },
  textContainer: { 
    backgroundColor: '#EF5350', 
    justifyContent: 'center', 
    borderRadius: 2,
  },
  text: {
    backgroundColor: '#EF5350',
    fontSize: 14, 
    color: '#fff', 
    padding: 10,
  },
  arrow: { 
    backgroundColor: '#EF5350', 
    height: 12, 
    width: 12, 
    borderRadius: 2, 
    transform: [{ translateY: -7 }, { rotateZ: '45deg' }]
  }
})

export default withBounce(Error)
