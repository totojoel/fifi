import React from 'react'
import { withNavigation } from 'react-navigation'
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native'

const Header = ({ navigation }) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={() => navigation.push('NewPhrase')}>
      <Image style={styles.logo} source={require('../assets/icon.png')} />
    </TouchableOpacity>
  </View>
)

const styles = StyleSheet.create({
  logo: {
    height: 50,
    width: 50,
  },
  container: {
    paddingTop: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default withNavigation(Header)