import React from 'react'
import { StyleSheet, View, Image } from 'react-native'
import { Ionicons as Icon } from '@expo/vector-icons'

const Footer = () => (
  <View style={styles.container}>
    <Icon style={styles.icon} name='ios-add-circle' size={30} color='#FFB300'/>
    <Icon style={styles.icon} name='ios-search' size={30} color='#FFB300'/>
    <Icon style={styles.icon} name='ios-close-circle' size={30} color='#FFB300'/>
  </View>
)

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    paddingBottom: 20,
  },
  icon: {
  }
});

export default Footer