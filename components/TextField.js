import React from 'react'
import { StyleSheet, TextInput, View, TouchableOpacity } from 'react-native'
import { Ionicons as Icon } from '@expo/vector-icons'

const TextField = ({ style, icon, placeholder, value, onChange, clearable }) => (
  <View style={{...style, ...styles.container}}> 
    {icon && <Icon style={{ marginRight: 4 }} name='ios-search' size={20} color='#6D4C41'/>}
    <TextInput 
      style={styles.textInput}
      placeholder={placeholder} 
      value={value}
      onChangeText={onChange}
    />
    {clearable && <TouchableOpacity onPress={() => onChange('')}>
      <Icon style={{ marginLeft: 4 }} name='md-close' size={20} color='#D7CCC8'/>
    </TouchableOpacity>}
  </View>
)

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    padding: 8,
    paddingLeft: 0,
    paddingRight: 0,
    borderBottomWidth: 1,
    alignItems: 'center',
    borderColor: '#6D4C41',
  },
  textInput: {
    flex: 1,
    fontSize: 20,
    color: '#6D4C41',
    fontFamily: 'roboto',
    fontWeight: 'normal'
  }
})

export default TextField
