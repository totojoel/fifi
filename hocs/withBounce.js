import React, { useState, useEffect } from 'react'
import posed from 'react-native-pose'

const Box = posed.View({
  visible: { 
    y: 0, 
    opacity: 1,
    delayChildren: 100,
    staggerChildren: 200,
    transition: {
      y: { type: 'spring', stiffness: 2000, damping: 15 },
      default: { duration: 500 }
    }
  },
  hidden: { y: -5, opacity: 0 }
})

const withBounce = (Wrapped, style) => (
  (props) => {
    const [visible, setVisible] = useState(false)

    useEffect(() => {
      const timeout = setTimeout(() => {
        setVisible(true)
      }, props.delay ? props.delay * 100 : 1)

      return () => {
        clearTimeout(timeout)
      }
    }, [])

    return (
      <Box style={style} pose={ visible ? 'visible' : 'hidden' }>
        <Wrapped {...props} />
      </Box>
    )
  }
)

export default withBounce
