import React from 'react'
import { View, StyleSheet } from 'react-native'

import Header from '../components/Header'
import Footer from '../components/Footer'


const withLayout = (Wrapped) => (
  () => (
    <View style={styles.container}>
      <Header />
      <Wrapped />
      {/* <Footer /> */}
    </View>
  )
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EFEBE9',
  }
})

export default withLayout