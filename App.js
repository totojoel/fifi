import React, { useEffect, useState } from 'react'
import { ApolloProvider } from 'react-apollo-hooks'
import { Text } from 'react-native'
import * as Font from 'expo-font'

import createClient from './libs/createClient'
import Main from './Main'

const App = () => {
  const [fontsLoaded, setFontsLoaded] = useState(false)

  useEffect(() => {
    Font.loadAsync({
      'roboto-slab': require('./assets/fonts/roboto-slab/RobotoSlab-Light.ttf'),
      'roboto': require('./assets/fonts/roboto/Roboto-Light.ttf')      
    })
    .then(() => {
      setFontsLoaded(true)
    })  
  }, [])

  return (
    fontsLoaded ? <ApolloProvider client={createClient('')}>
      <Main />
    </ApolloProvider> : <Text>loading</Text>
  )
}

export default App
