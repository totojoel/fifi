import React, { useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Ionicons as Icon } from '@expo/vector-icons'
import withLayout from '../hocs/withLayout'
import withBounce from '../hocs/withBounce'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import * as FileSystem from 'expo-file-system'
import gql from 'graphql-tag'
import { Formik } from 'formik'
import * as yup from 'yup'
import { useMutation } from 'react-apollo-hooks'

import TextField from '../components/TextField'
import Error from '../components/Error'

const ADD_PHRASE = gql`
  mutation($phrase: [phrase_insert_input!]!) {
    insert_phrase(objects: $phrase) {
      returning {
        id
      }
    }
  }
`

const NewPhrase = ({ navigation }) => {
  const [addPhrase,] = useMutation(ADD_PHRASE)

  useEffect(() => {
    this.getPermissionAsync()
  }, [])
  
  getPermissionAsync = async () => {
    const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
    if (permission.status !== 'granted') {
        const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (newPermission.status === 'granted') {
        }
    }
  }

  pickImage = async (onChange, onError, onTouch) => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    });
    
    if (!result.cancelled) {
      onTouch()
      const localImage = result.uri.split('/').pop()
      const format = localImage.split('.').pop()
      if (format === 'gif') {
        let imageUri = await FileSystem.readAsStringAsync(result.uri, {
          encoding: FileSystem.EncodingType.Base64,
        })
        imageUri = 'data:image/gif;base64,' + imageUri
        onChange(imageUri)
      } else  {
        onError('Please upload a .gif!')
      }
    }
  }

  return (
    <Formik
      initialValues={{
        text: '',
        translation: ''
      }}
      validationSchema={yup.object().shape({
        text: yup
          .string()
          .required('Text is required'),
        translation: yup
          .string()
          .required('Translation is required')
      })}
      onSubmit={(values, { setSubmitting, setStatus, resetForm }) => {
        addPhrase({ variables: { phrase: { ...values } } })
          .then(() => {
            setSubmitting(false)
            resetForm()
          })
          .catch((error) => {
            setStatus(error)
            setSubmitting(false)
          })
      }}
    >
      {({ values, errors, touched, setFieldTouched, setFieldValue, setFieldError, handleSubmit, isSubmitting, status }) => (
        <View style={styles.container}>
          <TouchableOpacity style={styles.closeIcon} onPress={() => navigation.navigate('Home')}>
            <Icon name='md-close' size={20} color='#D7CCC8'/>
          </TouchableOpacity>
          <Text style={styles.title}>New translation</Text>
          {(touched.text && errors.text) && <Error text={errors.text} />}
          <TextField 
            value={values.text}
            placeholder='Type a word or phrase'
            onChange={(value) => setFieldValue('text', value)}
          />
          {(touched.translation && errors.translation) && <Error text={errors.translation} />}
          <TouchableOpacity style={styles.image} onPress={() => {
            const onChange = (value) => setFieldValue('translation', value)
            const onError = (error) => setFieldError('translation', error)
            const onTouch = () => setFieldTouched('translation', true, false)
            pickImage(onChange, onError, onTouch)
          }}>
            {values.translation 
              ? <Image 
                  source={{ uri: values.translation }}
                  style={{ width: '100%', height: undefined, aspectRatio: 1 }}
                />
              : <Icon style={{ alignSelf: 'center' }} name='ios-image' size={40} color='#D7CCC8'/>
            }
          </TouchableOpacity>
          {status && <Error text='Failed adding new translation' />}
          <TouchableOpacity style={styles.button} onPress={handleSubmit}>
            <Text style={{ fontSize: 20, color: '#fff', fontFamily: 'roboto-slab' }}>{
              isSubmitting ? 'Submitting' : 'Submit'
            }</Text>
          </TouchableOpacity>
        </View>
      )}
    </Formik>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 40, 
    marginTop: 20,
    padding: 20,
    color: '#fff',
    display: 'flex',
    borderRadius: 2,
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: '#D7CCC8',
  },
  button: {
    padding: 8,
    marginTop: 10,
    paddingLeft: 0,
    paddingRight: 0,
    borderRadius: 2,
    backgroundColor: '#FFB300',
    alignItems: 'center',
    fontFamily: 'roboto',
    fontWeight: 'normal'
  },
  image: {
    borderRadius: 2,
    flex: 1,
    justifyContent: 'center',
    display: 'flex',
    backgroundColor: '#FAFAFA',
    marginTop: 10,
  },
  closeIcon: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 10,
  },
  title: {
    fontSize: 26,
    color: '#6D4C41',
    marginBottom: 10, 
    fontWeight: '500',
    fontFamily: 'roboto-slab',
    fontWeight: 'normal',
  }
})

export default withLayout(withBounce(withNavigation(NewPhrase), style={flex: 1}))
