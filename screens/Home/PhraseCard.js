import React from 'react'
import { StyleSheet, TouchableOpacity, Text } from 'react-native'
import { withNavigation } from 'react-navigation'
import withBounce from '../../hocs/withBounce'

const PhraseCard = ({ id, text, navigation }) => {
  return (
    <TouchableOpacity onPress={() => {
      navigation.navigate('PhraseDetails', { id }) 
    }} style={styles.container}> 
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  text: {
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    fontSize: 20,
    fontFamily: 'roboto',
    color: '#6D4C41'
  },
  container: {
    marginRight: 10,
    marginBottom: 10,
    borderRadius: 1,
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: '#D7CCC8',
    alignSelf: 'flex-start',
  }
})

export default withNavigation(withBounce(PhraseCard))
