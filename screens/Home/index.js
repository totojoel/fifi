import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import gql from 'graphql-tag'
import { useSubscription } from 'react-apollo-hooks'

import withLayout from '../../hocs/withLayout'
import TextField from '../../components/TextField'
import PhraseCard from './PhraseCard'

const SEARCH_PHRASE = gql`
  subscription ($text: String) {
    results: phrase(
      where: { text: { _ilike: $text } },
      order_by: { text: asc }
    ) {
      id
      text
    }
  }
`


const Results = ({ data }) => {
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={styles.results}>
      {
        data && data.map((item, index) => (
          <PhraseCard
            delay={index}
            key={item.id}
            id={item.id}
            text={item.text}
          />
        ))
      }
      </View>
    </ScrollView>
  )
}


const Finder = () => {
  const [text, setText] = useState('')
  const { data, loading, error } = useSubscription(SEARCH_PHRASE, {
    variables: { text: `%${text}%` }
  })

  return (
    <View style={styles.container}>
      <TextField 
        icon='search'
        value={text}
        placeholder='Type a word or phrase'
        onChange={setText}
        clearable
      />
      <Text style={styles.title}>
        {!!text ? `${data && data.results ? data.results.length : 'No'} matches for "${text}"` : 'All phrases'}
      </Text>
      { loading && <Text>loading</Text> }
      { error && <Text>{error.message}</Text> }
      { data && data.results && <Results data={data.results} />}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40, 
    paddingTop: 20,
    color: '#fff',
    display: 'flex',
  },
  results: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  title: {
    fontSize: 26,
    color: '#6D4C41',
    fontWeight: '500',
    fontFamily: 'roboto-slab',
    fontWeight: 'normal',
    marginBottom: 10,
    marginTop: 20,
  }
});

export default withLayout(Finder)