import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import gql from 'graphql-tag'
import { withNavigation } from 'react-navigation'
import { useQuery } from 'react-apollo-hooks'
import { Ionicons as Icon } from '@expo/vector-icons'
import * as Speech from 'expo-speech'
import withLayout from '../hocs/withLayout'
import withBounce from '../hocs/withBounce'

const VIEW_TRANSLATION = gql`
  query ($id: uuid!) {
    result: phrase(
      where: { id: { _eq: $id } },
    ) {
      id
      text
      translation
    }
  }
`

const PhraseDetails = ({ navigation }) => {
  const { data, loading, error } = useQuery(VIEW_TRANSLATION, {
    variables: { id: navigation.getParam('id', null) }
  })

  const phrase = data ? data.result[0] : null

  return (
    <View style={styles.container}>
      { loading && <Text>loading</Text> }
      { error && <Text>{error.message}</Text> }
      { phrase && 
        <>
          <TouchableOpacity style={styles.closeIcon} onPress={() => navigation.navigate('Home')}>
            <Icon name='md-close' size={20} color='#D7CCC8'/>
          </TouchableOpacity>
          <TouchableOpacity style={{ 
            display: 'flex',
            alignItems: 'center',
            flexWrap: 'wrap',
            flexDirection: 'row',
           }} onPress={() => {
            Speech.speak(phrase.text)
          }}>
            <Icon style={{ marginTop: -4 }} name='ios-volume-high' size={26} color='#D7CCC8'/>
            <Text style={styles.title}>{phrase.text}</Text>
          </TouchableOpacity>
          {phrase.translation && <View style={{ display: 'flex', flex: 1, justifyContent: 'center' }}>
            <Image
              source={{ uri: phrase.translation }}
              style={{ width: '100%', height: undefined, aspectRatio: 1 }}
            />
          </View>}
        </>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 40, 
    marginTop: 20,
    padding: 20,
    color: '#fff',
    display: 'flex',
    borderRadius: 2,
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: '#D7CCC8',
  },
  closeIcon: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 10,
  },
  title: {
    fontSize: 26,
    color: '#6D4C41',
    marginBottom: 10, 
    marginLeft: 10,
    fontWeight: '500',
    fontFamily: 'roboto-slab',
    fontWeight: 'normal',
  }
})

export default withLayout(withBounce(withNavigation(PhraseDetails), style={flex: 1}))
