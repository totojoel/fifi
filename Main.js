import { createStackNavigator, createAppContainer } from  'react-navigation'

import Home from './screens/Home'
import PhraseDetails from './screens/PhraseDetails'
import NewPhrase from './screens/NewPhrase'

const navigationOptions = {
  header: null
}

const MainStack = createStackNavigator({
  Home: { screen: Home, navigationOptions },
  PhraseDetails: { screen: PhraseDetails, navigationOptions },
  NewPhrase: { screen: NewPhrase, navigationOptions }
},
{
  initialRouteName: 'Home',
  transitionConfig: () => ({
    transitionSpec: {
      duration: 0,  // disable default transition
    },
  }),
})

export default createAppContainer(MainStack)
